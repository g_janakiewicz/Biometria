import argparse


class Args_Parser(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("-cf", "--create_feature_set", action = "store_true",
                                 help = "Extract features from set of photographs and create CSV file")
        self.parser.add_argument("-tn", "--train_neural_net", action = "store_true",
                                 help = "Train Neural Network from CSV file, set in the config file")
        self.parser.add_argument("-p", "--predict", action = "store_true",
                                 help = "Make predictions on a given Photographs by a trained Neural Network")
        self.parser.add_argument("-oc", "--output_csv_file",
                                 help = "Output CSV file to be created")
        self.args = self.parser.parse_args()

    def get_parser(self):
        return self.args
