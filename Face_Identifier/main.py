#!/usr/bin/env python2

import os
import glob
import numpy
from args_parser import args_parser
from csv_creator import csv_creator
from identifier_config import identifier_config
from features_extractor import features_extractor
from neural_classifier import neural_classifier


if __name__ == "__main__":
    Parser = args_parser.Args_Parser()
    args = Parser.get_parser()
    c_creator = csv_creator.Csv_Creator(identifier_config.SVM["RESOURCES"]["PATHS"]["FACES_FEATURES_CSV"])

    F_EXTRACTOR = features_extractor.Features_Extractor(
        identifier_config.SVM["MODELS"]["MODELS_PATH"] + identifier_config.SVM["MODELS"]["PREDICTOR"],
        identifier_config.SVM["MODELS"]["MODELS_PATH"] + identifier_config.SVM["MODELS"]["FACE_REC"])
    N_CLASSIFIER = neural_classifier.Neural_Classifier()
    N_CLASSIFIER.init_neural_network()

    # Features extraction
    if args.create_feature_set:
        # Extract feaures from my face
        F_EXTRACTOR.extract_and_classify_features_from_set(identifier_config.SVM["RESOURCES"]["PATHS"]["FACES_TRUE"],
                                                           identifier_config.SVM["RESOURCES"]["RESPONSES"]["FACES_TRUE"],
                                                           c_creator)

        # Extract features from faces other, than mine
        F_EXTRACTOR.extract_and_classify_features_from_set(identifier_config.SVM["RESOURCES"]["PATHS"]["FACES_FALSE"],
                                                           identifier_config.SVM["RESOURCES"]["RESPONSES"]["FACES_FALSE"],
                                                           c_creator)
    # Classification
    N_CLASSIFIER.load_data(identifier_config.SVM["RESOURCES"]["PATHS"]["FACES_FEATURES_CSV"])
    N_CLASSIFIER.train_network(EPOCHS = 75, BATCH_SIZE = 10)
    N_CLASSIFIER.evaluate_network()

    # Predictions
    print("\n[ main ] INFO: Test prediction on a trained ANN\n")
    #MY_FACE = F_EXTRACTOR.extract_features_from_photo("path/to/jpg")
    #MY_FACE = F_EXTRACTOR.extract_features_from_photo("/path/to/my/jpg")
    #OTHER_FACE = F_EXTRACTOR.extract_features_from_photo("/path/to/not/my/jpg")

    #MF = numpy.array(MY_FACE)
    #OF = numpy.array(OTHER_FACE)

    #MF = MF.reshape((1, 128))
    #OF = OF.reshape((1, 128))


    #MY_FACE_PRED = N_CLASSIFIER.predict(MF)
    #print("[ main ] INFO: My face test prediction: {p}".format(p=MY_FACE_PRED))

    #OTHER_FACE_PRED = N_CLASSIFIER.predict(OF)
    #print("[ main ] INFO: Other face test prediction: {p}".format(p=OTHER_FACE_PRED))

    while True:
        PHOTO_PATH = raw_input("\n[ Prediction ] Path to photograph (q ends program): ")
        if PHOTO_PATH.lower() == "q":
            break

        if "jpg" not in PHOTO_PATH:
            for IMG in glob.glob(os.path.join(PHOTO_PATH, "*jpg")):
                FACE = F_EXTRACTOR.extract_features_from_photo(IMG)
                FA = numpy.array(FACE)
                FA = FA.reshape((1, 128))
                PREDICTION = N_CLASSIFIER.predict(FA)
                print("[ prediction ]: Face prediction: {p}".format(p = PREDICTION))
        else:
            FACE = F_EXTRACTOR.extract_features_from_photo(PHOTO_PATH)
            FA = numpy.array(FACE)
            FA = FA.reshape((1, 128))
            PREDICTION = N_CLASSIFIER.predict(FA)
            print("[ prediction ] Face prediction: {p}".format(p = PREDICTION))


