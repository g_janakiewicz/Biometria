import csv
import argparse

class Args_Parser(object):
    def __init__(self):
        self.parser = argparse.ArgumentParser()
        self.parser.add_argument("-of", "--output_csv_file",
                                 help = "Output CSV file to be created")
        self.args = self.parser.parse_args()

    def get_parser(self):
        return self.args


class Csv_Creator(object):
    def __init__(self, output_file):
        self.OUTPUT_FILE = output_file
        self.DELIMITER = ","

    def append_line_to_file(self, data):
        if data is not None:
            with open(self.OUTPUT_FILE, 'a') as OF:
                writer = csv.writer(OF, delimiter = self.DELIMITER)
                writer.writerow(data)

    def append_classified_line_to_file(self, data, classification):
        '''
        Writes a comma separated data row to a file with the data classification
        integer as the last element
        '''
        if data is not None:
            with open(self.OUTPUT_FILE, 'a') as OF:
                writer = csv.writer(OF, delimiter = self.DELIMITER)
                writer.writerow(list(data) + [classification])
