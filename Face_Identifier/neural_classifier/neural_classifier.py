from keras.models import Sequential
from keras.layers import Dense
import numpy
import os

class CsvNotFoundException(Exception):
    pass


class Neural_Classifier(object):
    def __init__(self, FEATURES_VECTOR_SIZE=0, RESPONSE_VECTOR_SIZE=0):
        numpy.random.seed(7)
        self.FEATURES_VECTOR_SIZE = FEATURES_VECTOR_SIZE
        self.RESPONSE_VECTOR_SIZE = RESPONSE_VECTOR_SIZE
        self.MODEL = None
        self.FEATURES = None
        self.RESPONSES = None

    def init_neural_network(self):
        self.MODEL = Sequential()
        self.MODEL.add(Dense(140, input_dim = 128, activation = 'relu'))
        self.MODEL.add(Dense(96, activation = 'relu'))
        self.MODEL.add(Dense(1, activation = 'sigmoid'))

        self.MODEL.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])

    def load_data(self, DATA_FILE):
        if os.path.isfile(DATA_FILE):
            DATASET = numpy.loadtxt(DATA_FILE, delimiter = ",")
            self.FEATURES = DATASET[:,0:128]
            self.RESPONSES = DATASET[:,128]
        else:
            raise CsvNotFoundException("\n\tFile [{f}] does not exist!".format(f = DATA_FILE))

    def train_network(self, EPOCHS = 75, BATCH_SIZE = 10):
        self.MODEL.fit(self.FEATURES, self.RESPONSES, epochs = EPOCHS, batch_size = BATCH_SIZE)

    def evaluate_network(self):
        SCORES = self.MODEL.evaluate(self.FEATURES, self.RESPONSES)
        print("\n%s: %.2f%%" % (self.MODEL.metrics_names[1], SCORES[1] * 100))

    def predict(self, FEATURE):
        PREDICTION = self.MODEL.predict(FEATURE)

        ROUNDED = [round(x[0]) for x in PREDICTION]
        print(ROUNDED)

        return PREDICTION

    def save_weights(self, FILE_NAME):
        self.MODEL.save_weights(FILE_NAME)

    def load_weights(self, FILE_NAME):
        self.MODEL.load_weights(FILE_NAME)

