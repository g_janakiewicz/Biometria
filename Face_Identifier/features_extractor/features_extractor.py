#!/usr/bin/env python2

import os
import sys
import dlib
import glob
from skimage import io


class Features_Extractor(object):
    def __init__(self, predictor, face_rec_model):
        self.DETECTOR = dlib.get_frontal_face_detector()
        self.SHAPE_PREDICTOR = dlib.shape_predictor(predictor)
        self.FACE_RECOGNIZER = dlib.face_recognition_model_v1(face_rec_model)
        self.SHOW_DETECTION = False
        self.IMG_WINDOW = dlib.image_window() if self.SHOW_DETECTION else None

    def extract_features_from_photo(self, photo_path):
        print("[Features_Extractor] INFO: Processing: \n\tImage: {p}".format(p = photo_path))
        FACE_DESCRIPTOR = None
        IMG = io.imread(photo_path)
        if self.SHOW_DETECTION:
            self.IMG_WINDOW.clear_overlay()
            self.IMG_WINDOW.set_image(IMG)

        DETS = self.DETECTOR(IMG, 1)
        print("\tNumber of faces detected: {n}".format(n = len(DETS)))

        for k, d in enumerate(DETS):
            print("\tDetection {}: Left: {}, Top: {}, Right: {}, Bottom: {}"
                  .format(k, d.left(), d.top(), d.right(), d.bottom()))
            SHAPE = self.SHAPE_PREDICTOR(IMG, d)

            if self.SHOW_DETECTION:
                self.IMG_WINDOW.clear_overlay()
                self.IMG_WINDOW.add_overlay(d)
                self.IMG_WINDOW.add_overlay(SHAPE)

            try:
                FACE_DESCRIPTOR = self.FACE_RECOGNIZER.compute_face_descriptor(
                    IMG, SHAPE)
            except Exception as e:
                print("\n[ Features_Extractor ] WARNING: Problem with: \n\t{p} \n\t{e}".format(p=photo_path, e=e))
                #print(FACE_DESCRIPTOR)

            return FACE_DESCRIPTOR

    def extract_features_from_photos_set(self, faces_dir_path):
        for img in glob.glob(os.path.join(faces_dir_path, "*.jpg")):
            FACE_DESCRIPTOR = self.extract_features_from_photo(img)
            dlib.hit_enter_to_continue()

    def extract_and_classify_features_from_set(self,
                                               faces_dir_path,
                                               classification,
                                               csv_creator):
        for img in glob.glob(os.path.join(faces_dir_path, "*jpg")):
            FACE_DESCRIPTOR = self.extract_features_from_photo(img)
            if FACE_DESCRIPTOR:
                csv_creator.append_classified_line_to_file(FACE_DESCRIPTOR,
                                                           classification)

