SVM = {
    "MODELS": {
        "MODELS_PATH": "dat_files/",
        "PREDICTOR": "shape_predictor_5_face_landmarks.dat",
        "FACE_REC": "dlib_face_recognition_resnet_model_v1.dat"
    },

    "RESOURCES": {
        "PATHS": {
            "FACES_TRUE": "TWARZE/Moja/",
            "FACES_FALSE": "TWARZE/Nie_moja/",
            "FACES_FEATURES_CSV": "faces_classificators.csv"
        },
        "RESPONSES": {
            "FACES_TRUE": 1,
            "FACES_FALSE": 0
        }
    }
}
